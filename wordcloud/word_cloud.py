#!/usr/bin/python
from wordcloud import WordCloud, STOPWORDS
import os, pandas, argparse

if __name__ == '__main__':

    # create an ArgumentParser object
    parser = argparse.ArgumentParser()

    # provide input file
    parser.add_argument('-i', '--input', help='Input CSV file',
                        default='../Datasets-2018/train_set.csv')
    # provide output directory
    parser.add_argument('-o', '--output', help='Output directory',
                        default='generated_wordclouds/')
    # provide additional file for stopwords
    parser.add_argument('-s', '--stopwords', help='Stopwords file',
                        default='../stopwords.txt')

    # return an object with the above attributes
    # (input, output, stopwords)
    args = parser.parse_args()

    # check provided output directory name
    # add / in the end in case of missing
    dir_out = args.output
    if dir_out[-1] != '/':
        dir_out += '/'

    # info messages
    print 'Input file:', args.input
    print 'Output directory:', dir_out
    print 'Stopwords:', args.stopwords

    try:
        # read TAB separated file that provided as input
        # and assign it to a data frame using pandas
        df = pandas.read_csv(args.input, sep='\t')

        # create the output directory, if it doesn't exist already
        if not os.path.exists(dir_out):
            os.mkdir(dir_out, 0755)

        # words to ignore (stopwords)
        # default stopwords provided by the library
        stopwords = STOPWORDS.copy()
        # add additional stopwords provided by the file
        with open(args.stopwords) as stopw_f:
            for word in stopw_f:
                stopwords.add(word.rstrip())

        # create a dictionary:
        # key = category, value = content
        categories = dict()

        # gather all the content in one string for each category
        for index, row in df.iterrows():
            cat = row['Category']
            if cat not in categories:
                categories[cat] = ''
            categories[cat] += row['Content'] + ' '

        # remove stopwords and make wordcloud for each category
        for (cat, texts) in categories.iteritems():
            path_out = dir_out + cat + '.png'
            print 'Generating wordcloud for category', cat, 'at', path_out
            wc = WordCloud(width = 840, height = 420, stopwords=stopwords) \
                    .generate(texts)
            wc.to_file(path_out)

    # in case of IOError, just print it and exit
    except IOError as e:
        print 'Error:', str(e)
        exit(1)
