#!/usr/bin/python
import os, pandas, argparse, numpy, collections, time, warnings
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from gensim.models import Word2Vec
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_validate
from nltk.stem.snowball import SnowballStemmer

if __name__ == '__main__':

    # ignore warnings
    warnings.simplefilter("ignore")

    # create an ArgumentParser object
    parser = argparse.ArgumentParser()

    # provide train set file
    parser.add_argument('-t', '--train', help='Input train CSV file',
                        default='../Datasets-2018/train_set.csv')
    # provide test set file
    parser.add_argument('-i', '--test', help='Input test CSV file',
                        default='../Datasets-2018/test_set.csv')
    # provide stopwords file
    parser.add_argument('-s', '--stopwords', help='Stopwords file',
                        default='../stopwords.txt')

    # return an object with the above attributes
    # (train, test, stopwords)
    args = parser.parse_args()

    # info messages
    print 'Train set:', args.train
    print 'Test set:', args.test
    print 'Stopwords:', args.stopwords

    try:
        # read TAB separated files that provided as input
        # and assign it to data frames using pandas
        traindf = pandas.read_csv(args.train, sep='\t')
        testdf = pandas.read_csv(args.test, sep='\t')

        # create an english stemmer
        stemmer = SnowballStemmer("english")

        # make a list of the contents from train data
        trainContents = [line['Content'] for _, line in traindf.iterrows()]

        # make a list of lists from train data
        # one outer list entry for each article
        # one inner list entry for each word in the article
        trainWords = [line['Content'].split() for _, line in
                traindf.iterrows()]

        # make a list of the contents from train data + stem
        # dataset includes non-ascii characters, we have to decode
        stemTrainContents = [stemmer.stem(line['Content'].decode('utf8')) for _,
                line in traindf.iterrows()]

        # make a list of train categories
        categs = [line['Category'] for _, line in traindf.iterrows()]

        # build label encoder (from text to integer)
        labelenc = LabelEncoder()
        cat_encoded = labelenc.fit_transform(categs)

        # make a list of the contents from test data
        testContents = [line['Content'] for _, line in testdf.iterrows()]

        # make a list of lists from test data
        # one outer list entry for each article
        # one inner list entry for each word in the article
        testWords = [line['Content'].split() for _, line in testdf.iterrows()]

        # make a list of the contents from test data + stem
        # dataset includes non-ascii characters, we have to decode
        stemTestContents = [stemmer.stem(line['Content'].decode('utf8')) for _, line
                in testdf.iterrows()]

        # make a list of test ids
        testIds = [line['Id'] for _, line in testdf.iterrows()]

        # read the stopwords from the file
        with open(args.stopwords) as fstop:
            stopwords = [l.strip() for l in fstop.readlines()]

        # make CountVectorizer from train data (library stopwords)
        countVectorizer = CountVectorizer(stop_words='english')

        # make TfidfVectorizer from train data (file stopwords)
        tfidfVectorizer = TfidfVectorizer(stop_words=stopwords)

        # make BoW train data
        bowTrain = countVectorizer.fit_transform(trainContents)

        # make tfidf train data
        tfidfTrain = tfidfVectorizer.fit_transform(stemTrainContents)

        # make BoW test data
        bowTest = countVectorizer.transform(testContents)

        # make tfidf test data
        tfidfTest = tfidfVectorizer.transform(stemTestContents)

        # dimensionality reduction
        svdCount = TruncatedSVD(n_components = 100)
        svdTfidf = TruncatedSVD(n_components = 100)

        # make SVD train data
        svdTrain = svdCount.fit_transform(bowTrain)

        # make Beat the Benchmark train data
        btbTrain = svdTfidf.fit_transform(tfidfTrain)

        # make SVD test data
        svdTest = svdCount.transform(bowTest)

        # make Beat the Benchmark test data
        btbTest = svdTfidf.transform(tfidfTest)

        # make average W2V from train data
        train_model = Word2Vec(trainWords, min_count=1)
        train_x = []
        for doc in trainWords:
            sum_w2v = 0
            count_w2v = 0
            for word in doc:
                if word in train_model.wv.vocab:
                    sum_w2v += train_model.wv[word]
                    count_w2v += 1
            if count_w2v:
                train_x.append(sum_w2v/count_w2v)
            else:
                train_x.append(numpy.zeros(100))
        w2vTrain = numpy.array(train_x)

        # make average W2V from test data
        test_model = Word2Vec(testWords, min_count=1)
        test_x = []
        for doc in testWords:
            sum_w2v = 0
            count_w2v = 0
            for word in doc:
                if word in test_model.wv.vocab:
                    sum_w2v += test_model.wv[word]
                    count_w2v += 1
            if count_w2v:
                test_x.append(sum_w2v/count_w2v)
            else:
                test_x.append(numpy.zeros(100))
        w2vTest = numpy.array(test_x)

        # 1st output column
        evaluationresult = collections.OrderedDict({'Statistic_Measure':
        ['Accuracy', 'Precision', 'Recall']})

        # metrics
        scoring = ['accuracy', 'precision_weighted', 'recall_weighted']

        for clf, name, pointsTrain, pointsTest in [

            # SVM_BoW
            (SVC(), 'SVM_BoW', bowTrain, bowTest),
            # RndForest_BoW
            (RandomForestClassifier(), 'RndForest_BoW', bowTrain, bowTest),

            # SVM_SVD
            (SVC(), 'SVM_SVD', svdTrain, svdTest),
            # RndForest_SVD
            (RandomForestClassifier(), 'RndForest_SVD', svdTrain, svdTest),

            # SVM_W2V
            (SVC(), 'SVM_W2V', w2vTrain, w2vTest),
            # RndForest_W2V
            (RandomForestClassifier(), 'RndForest_W2V', w2vTrain, w2vTest),

            # My_Method
            (RandomForestClassifier(n_estimators=300, criterion='gini'),
                'My_Method', btbTrain, btbTest)
        ]:
            # info message
            print '\nStarting 10-fold Cross Validation using ' + name + '...'

            # start counting time
            start_time = time.time()

            # perform 10-fold cross validation
            scores = cross_validate(clf,
                    pointsTrain,
                    cat_encoded,
                    scoring=scoring,
                    cv=10)

            # calculate mean of each metric
            acc = scores['test_accuracy'].mean()
            pre = scores['test_precision_weighted'].mean()
            rec = scores['test_recall_weighted'].mean()

            # print metrics
            print '{} --> Accuracy: {}, Precision: {}, Recall: {}'.format(
                    name, str(acc)[:5], str(pre)[:5], str(rec)[:5])

            # concatenate output, one column for each combination
            evaluationresult.update({name: [acc, pre, rec]})

            # create the output directory, if it doesn't exist already
            out_dir = name + '/'
            if not os.path.exists(out_dir):
                os.mkdir(out_dir, 0755)

            # predict test data
            clf.fit(pointsTrain, cat_encoded)
            predics = labelenc.inverse_transform(clf.predict(
                pointsTest)).tolist()

            # write predictions to file
            print 'Writing predictions at', out_dir + 'testSet_categories.csv'
            predicsdf = pandas.DataFrame({'Test_Document_ID': testIds,
                                        'Predicted_Category': predics})
            predicsdf.to_csv(out_dir + 'testSet_categories.csv', sep='\t',
                    index=False)

            # calculate elapsed time
            elapsed_time = time.time() - start_time

            # print elapsed time
            print 'Time spent on', name + ': ' + str(elapsed_time) + 'seconds'

        # write summary to file
        print '\nWriting metrics summary to EvaluationMetric_10fold.csv...'
        evaldf = pandas.DataFrame(evaluationresult)
        evaldf.to_csv('EvaluationMetric_10fold.csv', sep='\t', index=False)

    # in case of IOError, just print it and exit
    except IOError as e:
        print 'Error:', str(e)
        exit(1)
