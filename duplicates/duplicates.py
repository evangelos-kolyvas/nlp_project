#!/usr/bin/python
import pandas, argparse, time, warnings
from datasketch import MinHash, MinHashLSH
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

if __name__ == '__main__':

    # ignore warnings
    warnings.simplefilter("ignore")

    # create an ArgumentParser object
    parser = argparse.ArgumentParser()

    # provide input file
    parser.add_argument('-i', '--input', help='Input CSV file',
                        default='../Datasets-2018/train_set.csv')
    # provide output file
    parser.add_argument('-o', '--output', help='Output CSV file',
                        default='duplicatePairs.csv')
    # provide stopwords file
    parser.add_argument('-s', '--stopwords', help='Stopwords file',
                        default='../stopwords.txt')
    # provide threshold for cosine similarity
    parser.add_argument('-t', '--threshold', help='Similarity threshold',
                        default=0.7)

    # return an object with the above attributes
    # (input, output, stopwords, threshold)
    args = parser.parse_args()
    args.threshold = float(args.threshold)

    # info messages
    print 'Input file:', args.input
    print 'Output file (Part 1): similarityAllPairs.csv'
    print 'Output file (Part 2):', args.output
    print 'Stopwords:', args.stopwords
    print 'Similarity threshold:', args.threshold

    try:
        # read TAB separated file that provided as input
        # and assign it to a data frame using pandas
        df = pandas.read_csv(args.input, sep='\t')

        # make a list of the contents for each article
        contents = [line['Content'] for _, line in df.iterrows()]

        # read the stopwords from the file
        with open(args.stopwords) as fstop:
            stopwords = [l.strip() for l in fstop.readlines()]

        # make a CountVectorizer
        countVectorizer = CountVectorizer(stop_words=stopwords)

        # learn the vocabulary dictionary and return term-document matrix
        X = countVectorizer.fit_transform(contents)

        ### Part 1: Compute similarity between all pairs (without LSH) ###
        print 'Part 1: Compute similarity between all pairs (without LSH)'

        # start counting time for Part 1
        start_time = time.time()

        # initialize counter list
        # one list entry for each percentage
        counter = [0 for i in range(101)]

        # iterate through all pairs and count similarity percentage
        for article1 in range(len(contents) - 1):
            for article2 in range(article1 + 1, len(contents)):
                sim = cosine_similarity(X[article1], X[article2])
                counter[int(sim * 100)] += 1

        # print Part 1 results
        tmp_str = 'Similarity Percentage\tNumber of Pairs Found'
        print tmp_str
        out_data = tmp_str + '\n'

        for i in range(101):
            tmp_str = str(i) + '\t' + str(counter[i])
            print tmp_str
            out_data += tmp_str + '\n'

        # write Part 1 results to a file
        print 'Writing data to similarityAllPairs.csv...'
        with open('similarityAllPairs.csv', 'w') as out_file:
            out_file.write(out_data)

        # calculate elapsed time in Part 1
        elapsed_time = time.time() - start_time

        # print elapsed time
        print 'Time spent on Part 1: ' + str(elapsed_time) + ' seconds'

        ### Part 2: Compute similarity between pairs in the same bucket (using LSH) ###
        print 'Part 2: Compute similarity between pairs in the same bucket (using LSH)'

        # start counting time for Part 2
        start_time = time.time()

        # number of permutations
        permutations = 128

        # overestimate using less similarity threshold for the same bucket
        if args.threshold - 0.4 < 0:
            lsh = MinHashLSH(threshold=0.0, num_perm=permutations)
        else:
            lsh = MinHashLSH(threshold=args.threshold - 0.4, num_perm=permutations)

        # create a dictionary:
        # key = index, value = (id, minHash)
        dictionary = dict()

        # fill up the dictionary
        for index, row in df.iterrows():

            # dataset includes non-ascii characters, we have to decode
            # split content into words
            words = row['Content'].decode('utf8').split()

            # one minHash for each article
            minHash = MinHash(num_perm=permutations)

            for w in words:
                minHash.update(w.encode('utf8'))

            # one dictionary entry for each article
            dictionary[index] = (row['Id'], minHash)

            # update MinHashLSH
            lsh.insert(index, minHash)

        # output columns
        tmp_str = 'Document_ID1\tDocument_ID2\tSimilarity'
        print tmp_str
        out_data = tmp_str + '\n'

        # for each article
        for (this_index, (this_id, this_minHash)) in dictionary.iteritems():
            # find all articles in the same bucket
            bucket = lsh.query(this_minHash)
            # for every article in the same bucket
            for that_index in bucket:
                # if that_index is greater than this_index
                # avoid printing the same pair twice
                if that_index > this_index:
                    # find cosine similarity
                    sim = cosine_similarity(X[this_index], X[that_index])[0][0]
                    # if cosine similarity value is above the threshold
                    if sim > args.threshold:
                        # retrieve that_id from the dictionary
                        (that_id, that_minHash) = dictionary.get(that_index)
                        # concatenate output
                        tmp_str = str(this_id) + '\t' + str(that_id) + '\t' + str(sim)
                        print tmp_str
                        out_data += tmp_str + '\n'

        # write Part 2 results to a file
        print 'Writing data to ' + args.output + '...'
        with open(args.output, 'w') as out_file:
            out_file.write(out_data)

        # calculate elapsed time in Part 2
        elapsed_time = time.time() - start_time

        # print elapsed time
        print 'Time spent on Part 2: ' + str(elapsed_time) + ' seconds'

    # in case of IOError, just print it and exit
    except IOError as e:
        print 'Error:', str(e)
        exit(1)
